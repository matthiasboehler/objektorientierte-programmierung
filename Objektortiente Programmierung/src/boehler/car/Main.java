package boehler.car;

import java.time.LocalDate;
import java.util.Date;


public class Main {
	public static void main(String[] args) {
	
		Producer p1 = new Producer("Mercedes","Germany",10);
		
		Engine e1 = new Engine(200, "Benzin");
		
	
		Person per1 = new Person("Matthias", "Boehler", 2002, 9, 21);
		
		Car c1 = new Car("Black",300,40000,10,75000,e1,p1, per1);
		
		System.out.println(c1.price());
		System.out.println(c1.getBasicConsumption());
		System.out.println(per1.getBDayAsString());
		
	}
	 
}
