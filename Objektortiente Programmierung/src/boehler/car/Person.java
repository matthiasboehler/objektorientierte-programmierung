package boehler.car;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Person {
	
	private String fName;
	private String lName;
	private LocalDate bday;
	private List<Car> cars;
	
	public Person(String fName, String lName, int year, int month, int day) {
		super();
		this.fName = fName;
		this.lName = lName;
	    this.bday = LocalDate.of(year,month, day);
	    this.cars = new ArrayList<>();
	}
	
	public void addCar(Car c) {
		this.cars.add(c);
	}
	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getBDayAsString() {
		  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy mm dd");
		  String text = this.bday.format(formatter);
		  return text;
	}
	public LocalDate getBday() {
		return bday;
	}

	public void setBday(LocalDate bday) {
		this.bday = bday;
	}
	
	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	

}
