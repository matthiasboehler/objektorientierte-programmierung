package boehler.games.wintergames;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	public List<Snowflake> snowflakes;
	public MainGame(String title) {
		super(title);
 }

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		//zeichnet
		for (Snowflake snowflake : this.snowflakes) {
			snowflake.render(graphics);
			}
	}


	@Override
	public void init(GameContainer gc) throws SlickException {
		this.snowflakes = new ArrayList<Snowflake>();
		
		for (int i = 0; i < 40; i++) {
			snowflakes.add(new Snowflake(0)); 
		}
		for (int i = 0; i < 40; i++) {
			snowflakes.add(new Snowflake(1)); 
		}
		for (int i = 0; i < 40; i++) {
			snowflakes.add(new Snowflake(2)); 
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		
		for (Snowflake snowball: this.snowflakes) {
			snowball.update(gc, delta);
  }
 }

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} 
		
	catch (SlickException e) {
		e.printStackTrace();
  }
 }

}