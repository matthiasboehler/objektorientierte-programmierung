package boehler.remote;

public class Main {
	
		public static void main(String[] args) {
			
			Battery b1 = new Battery(70);
			Battery b2 = new Battery(50);
			
			Remote r1 = new Remote(false,true,b1,b2);
			
			System.out.println(r1);
			System.out.println(r1.isOn());
			System.out.println(r1.getStatus());
			System.out.println(r1.hasPower());
			r1.turnOn();
			System.out.println(r1.isOn());
		}
}
