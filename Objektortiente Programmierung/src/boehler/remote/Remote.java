package boehler.remote;

public class Remote {
	
	private boolean isOn;
	private boolean hasPower;
	private Battery battery;
	private Battery battery1;
	
	public Remote(boolean isOn, boolean hasPower, Battery battery, Battery battery1) {
		
		super();
		this.isOn = isOn;
		this.hasPower = hasPower;
		this.battery = battery;
		this.battery1 = battery1;
	}

	public void turnOn() {
		this.isOn = true;
	}

	public void turnOff() {
		this.isOn = false;;
	}

	public boolean isOn() {
		return isOn;
	}
	
	public int getStatus() {
		int status;
		status = (battery.getChargingStatus() + battery1.getChargingStatus()) / 2;
		return status;
	}
	
	public boolean hasPower() {
		
		if (getStatus() > 0)
			{
			this.hasPower = true;
			}
		
		else {
			this.hasPower = false;
			 }
		
		return hasPower;
		}
}
