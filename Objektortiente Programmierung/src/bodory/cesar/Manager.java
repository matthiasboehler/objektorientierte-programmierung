package bodory.cesar;

import java.util.ArrayList;
import java.util.List;

public class Manager {
	
	private List<Encrypter> encrypter;
	
	public Manager() {
		encrypter = new ArrayList<>();
	}
	
	public void encryptAll(String txt) {
		for (Encrypter e : encrypter) {
			e.encrypt(txt);
		}
	}
	
	public void addEncrypter(Encrypter e) {
		this.encrypter.add(e);
	}
	
}
