package bodory.cesar;

public class Main {
	public static void main(String[] args) {
		
		Manager m1 = new Manager();
		Encrypter c1 = new Cesar("s");
		
		m1.addEncrypter(c1);
		c1.encrypt("Juhu");
	}
}
