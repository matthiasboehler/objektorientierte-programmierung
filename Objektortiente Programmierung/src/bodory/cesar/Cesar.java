package bodory.cesar;

public class Cesar extends Abstract_Encrypter {
	
	private String founder = "Matthias";

	public Cesar(String founder) {
		super(founder);
	}

	@Override
	public void encrypt(String txt) {
		
		StringBuffer result= new StringBuffer(); 
		  
        for (int i=0; i<txt.length(); i++) 
        { 
            if (Character.isUpperCase(txt.charAt(i))) 
            { 
                char ch = (char)(((int)txt.charAt(i) + 
                                  1 - 65) % 26 + 65); 
                result.append(ch); 
            } 
            else
            { 
                char ch = (char)(((int)txt.charAt(i) + 
                                  1 - 97) % 26 + 97); 
                result.append(ch); 
            } 
        } 
		System.out.println(result);
	}

	@Override
	public String getFounder() {
		
		return("Wurde programmiert von " + founder);
	}
}
