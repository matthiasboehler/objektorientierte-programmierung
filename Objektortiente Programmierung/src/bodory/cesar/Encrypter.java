package bodory.cesar;

public interface Encrypter {
	
	public void encrypt(String txt);
	public String getFounder();
	
}