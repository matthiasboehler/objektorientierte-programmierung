package bodory.observer;

public interface Observer {
	public void inform();
}
