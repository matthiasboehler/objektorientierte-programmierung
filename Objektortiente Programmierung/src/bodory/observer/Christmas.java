package bodory.observer;

public class Christmas implements Observer {
	public void inform() {
		System.out.println("Christmas lights on!");
	}

}
