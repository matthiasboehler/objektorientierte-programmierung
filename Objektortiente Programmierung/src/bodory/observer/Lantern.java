package bodory.observer;

public class Lantern implements Observer {
	public void inform() {
		System.out.println("Lantern lights on!");
	}

}
