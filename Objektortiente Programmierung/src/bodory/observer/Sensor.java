package bodory.observer;

import java.util.ArrayList;
import java.util.List;

public class Sensor {
	private List<Observer> observers;
	
	public Sensor() {
		this.observers = new ArrayList<>();
	}
	
	public void informAll() {
		for (Observer o : observers) {
			o.inform();
		}
	}
	
	public void addObserver(Observer o) {
		this.observers.add(o);
	}
}
