package bodory.observer;

public class TrafficLight implements Observer{
	public void inform() {
		System.out.println("Traffic lights on!");
	}

}
