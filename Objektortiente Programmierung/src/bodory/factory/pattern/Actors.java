package bodory.factory.pattern;

public interface Actors {
	
	public void name();
	
	public void render();
}
