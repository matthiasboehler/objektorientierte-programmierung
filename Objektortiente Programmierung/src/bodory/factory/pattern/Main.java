package bodory.factory.pattern;

public class Main {
	public static void main(String[] args) {
		Game g = new Game();
		
		g.addRandomActor();
		g.addRandomActor();
		g.addRandomActor();
		g.addRandomActor();
		g.nameAll();
	}
}
