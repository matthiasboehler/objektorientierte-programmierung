package bodory.interfaces;
import java.util.ArrayList;
import java.util.List;

public class Player {
	public List<Playable> playables;
	public void play() {
		this.playables = new ArrayList<Playable>();
	}
	
	public void addPlayable(Playable p) {
		this.playables.add(p);
	}

	
	public void playAll() {
		for (Playable playable : playables) {
			playable.play();
		}
	}


	
}
