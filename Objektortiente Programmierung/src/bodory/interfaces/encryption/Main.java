package bodory.interfaces.encryption;

public class Main {
	
	public static void main(String[] args) {
		
		Manager m = new Manager();
		Encrypter e = new Caeser();
		
		Encrypter a1 = new Alg1();
		
		
		m.setEncrypter(e);
		m.encrypt("test");
		m.setEncrypter(a1);
		m.encrypt("hallo");
		
		e.getFounder();
	}
}
