package bodory.interfaces;

public interface Playable {
	public void play();
}
