package bodory.algorithms;

public class Sorter {
	private Algorithm algorithm;
	
	public Sorter() {
		
	}
	
	public void setAlgorithm(Algorithm a) {
		this.algorithm = a;
	}
	
	public int[] doSort(int[] unsortedArray) {
		int[] sorted = algorithm.doSort(unsortedArray);
		return sorted;
	}
}
