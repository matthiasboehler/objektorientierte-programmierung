package bodory.algorithms;

import java.util.Arrays;

public class Main {
	
	public static void main(String[] args) {
		Sorter s1 = new Sorter();
		BubbleSort b = new BubbleSort();
		InsertionSort i = new InsertionSort();
		SelectionSort s = new SelectionSort();
		
		s1.setAlgorithm(s);
		int[] unsortedArray = new int[]{ 1,67,2,11,7,4,16 }; 
		
		System.out.println(Arrays.toString(s1.doSort(unsortedArray)));
	
	}
}

