package bodory.algorithms;

public class SelectionSort implements Algorithm {

	public int[] doSort(int[] unsorted) {
		int n = unsorted.length; 
		  
        for (int i = 0; i < n-1; i++) 
        { 
            int min_index = i; 
            for (int j = i+1; j < n; j++) 
                if (unsorted[j] < unsorted[min_index]) 
                    min_index = j; 
  
            int temp = unsorted[min_index]; 
            unsorted[min_index] = unsorted[i]; 
            unsorted[i] = temp; 
        }
        return unsorted;
	}
}


