package bodory.algorithms;

public interface Algorithm {
	
		public int[] doSort(int[] unsorted);
		
}
