package at.bodory.vehicles;

public class Boat extends Vehicles {
	private String propeller;

	public Boat(String name, String color, String propeller) {
		super(name, color);
		this.propeller = propeller;
	}

	public String getPropeller() {
		return propeller;
	}

	public void setPropeller(String propeller) {
		this.propeller = propeller;
	}
	
}
