package at.bodory.vehicles;

public class Car extends Vehicles{
	private int amountOfTyres;

	public Car(String name, String color, int amountOfTyres) {
		super(name, color);
		this.amountOfTyres = amountOfTyres;
	}

	public int getAmountOfTyres() {
		return amountOfTyres;
	}

	public void setAmountOfTyres(int amountOfTyres) {
		this.amountOfTyres = amountOfTyres;
	}
	
}
